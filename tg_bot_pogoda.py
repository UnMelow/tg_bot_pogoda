import os
import io

import json
import requests
import time
from translate import Translator
from typing import Dict, Union, Optional

# API-ключи и настройки
OPENWEATHER_API_KEY = os.environ.get("OPENWEATHER_API_KEY")
YANDEX_SPEECH_API_KEY = os.environ.get("YANDEX_SPEECH_API_KEY")
TELEGRAM_BOT_TOKEN = os.environ.get("TELEGRAM_BOT_TOKEN")

# Этот словарь будем возвращать, как результат функции.
FUNC_RESPONSE = {
    'statusCode': 200,
    'body': ''
}
# URL для взаимодействия с Telegram Bot API
telegram_api_url = f"https://api.telegram.org/bot{TELEGRAM_BOT_TOKEN}/"

other_message = """
Я не могу ответить на такой тип сообщения.
Но могу ответить на:
    - Текстовое сообщение с названием населенного пункта.
    - Голосовое сообщение с названием населенного пункта.
    - Сообщение с точкой на карте.
    """
start_message = """
Я сообщу вам о погоде в том месте, которое сообщите мне.
Я могу ответить на:
    - Текстовое сообщение с названием населенного пункта.
    - Голосовое сообщение с названием населенного пункта.
    - Сообщение с точкой на карте.
Все другие команды я буду игнорировать.
"""
response_message = """
{description}.
Температура {temperature:.2f} ℃, ощущается как {feels_like:.2f} ℃.
Атмосферное давление {pressure} мм рт. ст.
Влажность {humidity} %.
Видимость {visibility} метров.
Ветер {wind_speed} м/с {wind_direction}.
Восход солнца {sunrise} МСК. Закат {sunset} МСК.
"""


def send_text_message(chat_id: Union[int, str], text: str) -> Dict[str, Union[int, str]]:
    """Отправляет текстовое сообщение через Telegram API"""
    payload = {
        "chat_id": chat_id,
        "text": text
    }
    response = requests.post(telegram_api_url + "sendMessage", json=payload)
    return response.json()


def translate_description(description: str) -> str:
    """Переводит описание погоды с английского на русский"""
    translator = Translator(to_lang="ru")
    translated = translator.translate(description)
    return translated


def send_voice_message(chat_id: Union[int, str], voice: bytes) -> Dict[str, Union[int, str]]:
    """
    Отправляет голосовое сообщение через Telegram API.

    Args:
        chat_id (Union[int, str]): Идентификатор чата в Telegram.
        voice (bytes): Содержимое голосового сообщения в виде байтов.

    Returns:
        Dict[str, Union[int, str]]: Ответ от Telegram API в виде словаря.
    """
    voice_file = io.BytesIO(voice)
    voice_file.name = "voice.ogg"
    files = {"voice": voice_file}
    payload = {"chat_id": chat_id}
    response = requests.post(telegram_api_url + "sendVoice", data=payload, files=files)
    print(response.json())
    if response.status_code == 200:
        return response.json()
    else:
        send_text_message(chat_id, "Ошибка при отправке голосового сообщения")


def generate_voice_message(text: str, chat_id: Union[int, str]) -> None:
    """Генерирует голосовое сообщение на основе текста"""
    url = "https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize"
    headers = {
        "Authorization": f"Bearer {YANDEX_SPEECH_API_KEY}"
    }
    data = {
        "text": text,
        "voice": "alena",
        "lang": "ru-RU",
        "emotion": "neutral"
    }

    response = requests.post(url, headers=headers, data=data)
    if response.status_code == 200:
        return response.content
    else:
        send_text_message(chat_id, "Ошибка при синтезе голосового сообщения")


def process_command(command: str, chat_id: Union[int, str]) -> None:
    """Обрабатывает полученные команды"""
    if command == "/start" or command == "/help":
        message = start_message
        send_text_message(chat_id, message)


def process_text_message(text: str, chat_id: Union[int, str]) -> None:
    """Обрабатывает полученное текстовое сообщение"""
    # Получение информации о погоде для указанного населенного пункта с использованием OpenWeatherMap API
    weather_info = get_weather_info_by_city(text)
    if weather_info:
        # Формирование ответного текстового сообщения
        response_text = generate_weather_response(weather_info)
        send_text_message(chat_id, response_text)
    else:
        error_message = f"Я не нашел населенный пункт \"{text}\"."
        send_text_message(chat_id, error_message)


def process_voice_message(file_id: str, chat_id: Union[int, str], duration: int) -> None:
    """Обрабатывает голосовые сообщения"""
    if duration > 30:
        response_text = "Я не могу понять голосовое сообщение длительностью более 30 секунд."
        send_text_message(chat_id, response_text)
        return

    voice_url = get_voice_url(file_id)
    if voice_url:
        recognized_text = recognize_speech(voice_url)
        if recognized_text:
            # Обработка распознанного текста
            weather_info = get_weather_info_by_city(recognized_text)
            if weather_info:
                response_text = generate_weather_response(weather_info)
                send_voice_message(chat_id, generate_voice_message(response_text, chat_id))
            else:
                response_text = f"Я не нашел населенный пункт \"{recognized_text}\"."
                send_text_message(chat_id, response_text)
        else:
            response_text = "Не удалось распознать голосовое сообщение."
            send_text_message(chat_id, response_text)
    else:
        response_text = "Не удалось получить голосовое сообщение."
        send_text_message(chat_id, response_text)


def process_location_message(location: Dict[str, float], chat_id: Union[int, str]) -> None:
    """Обрабатывает полученное сообщение с точкой на карте"""
    latitude = location["latitude"]
    longitude = location["longitude"]
    # Получение информации о погоде для указанных координат с использованием OpenWeatherMap API
    weather_info = get_weather_info_by_coordinates(latitude, longitude)
    if weather_info:
        # Формирование ответного текстового сообщения
        response_text = generate_weather_response(weather_info)
        send_text_message(chat_id, response_text)
    else:
        error_message = "Не удалось получить информацию о погоде для указанных координат. Попробуйте еще раз."
        send_text_message(chat_id, error_message)


def process_other_message(chat_id: Union[int, str]) -> None:
    """Обрабатывает полученное сообщение с типом, отличным от текстового, голосового и с точкой на карте"""
    message = error_message
    send_text_message(chat_id, message)


def handler(event, context):
    """Обработчик облачной функции. Реализует Webhook для Telegram Bot."""
    if TELEGRAM_BOT_TOKEN is None:
        return FUNC_RESPONSE

    update = json.loads(event['body'])

    if 'message' not in update:
        return FUNC_RESPONSE

    message_in = update['message']

    if 'text' in message_in:
        text = message_in['text']
        if text.startswith('/'):
            process_command(text, message_in['chat']['id'])
        else:
            process_text_message(text, message_in['chat']['id'])
    elif 'voice' in message_in:
        voice_message = message_in['voice']
        duration = voice_message.get("duration", 0)
        process_voice_message(voice_message['file_id'], message_in['chat']['id'], duration)
    elif 'location' in message_in:
        location = message_in['location']
        process_location_message(location, message_in['chat']['id'])
    else:
        process_other_message(message_in['chat']['id'])

    return FUNC_RESPONSE


def get_voice_url(file_id: str) -> Optional[str]:
    """Получает URL голосового сообщения по его идентификатору"""
    url = telegram_api_url + "getFile"
    payload = {
        "file_id": file_id
    }
    response = requests.get(url, params=payload)
    if response.status_code == 200:
        file_info = response.json()
        if "result" in file_info and "file_path" in file_info["result"]:
            file_path = file_info["result"]["file_path"]
            return f"https://api.telegram.org/file/bot{TELEGRAM_BOT_TOKEN}/{file_path}"
    return None


def recognize_speech(voice_url: str) -> Optional[str]:
    """Распознает речь с использованием Yandex SpeechKit API"""
    url = "https://stt.api.cloud.yandex.net/speech/v1/stt:recognize"
    headers = {
        "Authorization": f"Bearer {YANDEX_SPEECH_API_KEY}",
        "Content-Type": "audio/x-wav"
    }
    params = {
        "topic": "general",
        "lang": "ru-RU"
    }
    response = requests.post(url, headers=headers, params=params, data=requests.get(voice_url).content)
    if response.status_code == 200:
        result = response.json()
        if "result" in result:
            recognized_text = result["result"]
            return recognized_text
    return None


def get_wind_direction(degrees: int) -> str:
    """Определяет направление ветра по градусам"""
    if 337.5 <= degrees <= 360 or 0 <= degrees < 22.5:
        return "С"
    elif 22.5 <= degrees < 67.5:
        return "СВ"
    elif 67.5 <= degrees < 112.5:
        return "В"
    elif 112.5 <= degrees < 157.5:
        return "ЮВ"
    elif 157.5 <= degrees < 202.5:
        return "Ю"
    elif 202.5 <= degrees < 247.5:
        return "ЮЗ"
    elif 247.5 <= degrees < 292.5:
        return "З"
    elif 292.5 <= degrees < 337.5:
        return "СЗ"
    else:
        return "N/A"


def get_weather_info_by_city(city_name: str) -> Optional[Dict[str, Union[float, int]]]:
    """Получает информацию о погоде для указанного населенного пункта с использованием OpenWeatherMap API"""
    url = f"https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid={OPENWEATHER_API_KEY}&units=metric"
    response = requests.get(url)
    if response.status_code == 200:
        weather_data = response.json()
        return weather_data
    return None


def get_weather_info_by_coordinates(latitude: float, longitude: float) -> Optional[Dict[str, Union[float, int]]]:
    """Получает информацию о погоде для указанных координат с использованием OpenWeatherMap API"""
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPENWEATHER_API_KEY}&units=metric"
    response = requests.get(url)
    if response.status_code == 200:
        weather_data = response.json()
        return weather_data
    return None


def convert_timestamp_to_time(timestamp: int) -> str:
    """Преобразует метку времени в формат чч:мм"""
    time_struct = time.gmtime(timestamp)
    time_string = time.strftime("%H:%M", time_struct)
    return time_string


def generate_weather_response(weather_info: Dict[str, Union[float, int]]) -> str:
    """Генерирует ответное текстовое сообщение о погоде"""
    description = weather_info["weather"][0]["description"].capitalize()
    temperature = weather_info["main"]["temp"]
    feels_like = weather_info["main"]["feels_like"]
    pressure = weather_info["main"]["pressure"]
    humidity = weather_info["main"]["humidity"]
    visibility = weather_info.get("visibility", "N/A")
    wind_speed = weather_info["wind"]["speed"]
    wind_direction = weather_info["wind"].get("deg")
    if wind_direction:
        wind_direction = get_wind_direction(wind_direction)
    else:
        wind_direction = "N/A"
    sunrise = weather_info["sys"]["sunrise"]
    sunset = weather_info["sys"]["sunset"]

    response = response_message.format(description=translate_description(description), temperature=temperature,
                                       feels_like=feels_like,
                                       pressure=pressure, humidity=humidity, visibility=visibility,
                                       wind_speed=wind_speed,
                                       wind_direction=wind_direction, sunrise=convert_timestamp_to_time(sunrise),
                                       sunset=convert_timestamp_to_time(sunset))
    return response
